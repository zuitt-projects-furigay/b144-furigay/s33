const Course = require("../models/Course")
const auth = require("../auth")

module.exports.addCourse = (reqBody, userData) => {

    return Course.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Course creation successful"
                }
            })
        }
        
    });    
}



// module.exports.addCourse = (reqBody, userData) => {

//    return Course.findById(userData.userId).then(result => {

//        if (userData.isAdmin == false) {
//            return "You are not an admin"
//        } else {
//            let newCourse = new Course({
//                name: reqBody.name,
//                description: reqBody.description,
//                price: reqBody.price
//            })
       
//            //Saves the created object to the database
//            return newCourse.save().then((course, error) => {
//                //Course creation failed
//                if(error) {
//                    return false
//                } else {
//                    //course creation successful
//                    return "Course creation successful"
//                }
//            })
//        }
       
//    });    
// }



// -------------------------------------

// Get all courses

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}


// -------------------------------

// Retrieve all active courses

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}


// ------------------------------

// Rettieve specific course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}


// ------------------------------

// Update a Course

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error) {
			return false;
		}
		else {
			return true;
		}
	})
}


// ----------------------------------
// Archive

module.exports.archiveCourse = (reqParams, reqBody) => {
 let archivedCourse = {
   isActive : reqBody.isActive
 }
 return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course,error) => {
   if(error){
     return false;
   }
   else{
     return true;
   }
 })
}




