//Set up the dependencies
const express = require('express');
const mongoose = require('mongoose');

// Allows us to control app's Cross Original Resource Sharing setting
const cors = require('cors');

// Routes
const userRoutes = require('./routes/user')
const courseRoutes = require('./routes/course')


// Server setup

const app = express();
const port = 4001;

// Allows all resources/origin to access our backend application
// Enable all CORS request
app.use(cors())

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Defines the '/api/users' string to be included for all routes defined in tne 'user' route file
app.use('/api/users', userRoutes)
app.use('/api/courses', courseRoutes)


// DB connection
mongoose.connect("mongodb+srv://adriannfurigay:qwerty123@wdc028-course-booking.umyxd.mongodb.net/batch144_booking_system?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connected to the cloud database"))


// Routes(Base URI for task route)
// allows all the task routes created in the tasskRoutse.js file too use "/task" route




app.listen(port, () => console.log(`Now listening to port ${port}`))

