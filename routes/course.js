const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course")
const auth = require("../auth")




// Route for creating a course
router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    courseController.addCourse(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})



// // Route for creating a course
// router.post("/",auth.verify, (req,res) => {
// const userData = auth.decode(req.headers.authorization)
// courseController.addCourse(req.body, {
// userId: userData.id, isAdmin:userData.isAdmin
// })
// .then(resultFromController =>
// res.send(resultFromController))
// })


// ----------------------------------------



// Retrieve all courses

router.get("/all",(req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})


// --------------------------------------

// Retrieve all active corses

router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})
module.exports = router;

// ------------------------------

// Retrieve a specific Course

router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// --------------------------

// Update a Course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})


//-------------------------

//Archive

router.put("/:courseId/archive", auth.verify, (req,res) => {
courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})



// // Route for retrieving user datails
// router.get("/details", auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);

// 	// Provides the user's ID for the getProfile controller method
// 	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

// });

