const express = require('express');
const router = express.Router();
const auth = require("../auth")

const userController = require('../controllers/user')
// Route for checking if the user's email already exist in the database
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result))
})


// ------------------------------------------------


// Routes for user registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
})


// -----------------------------------------------

// Routes for authenticating a user
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
})


// -----------------------------------------------


// ACTIVITY CODE
// 

// router.post("/details/:id", (req, res) => {
// 	userController.getProfile(req.params.id).then(result => res.send(result))
// })

// Route for retrieving user datails
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});


// ----------------------------------------------------

// ACTIVITY

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId,

	}

	userController.enroll({data : userData}).then(resultFromController => res.send(resultFromController));
})






module.exports = router;





